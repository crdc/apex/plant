// Package cmd contains unit service definitions
//
// Apex plant unit service.
//
package cmd

import (
	"gitlab.com/crdc/apex/plant/pkg/service"

	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

// unitCmd represents the unit command
var (
	name string

	unitCmd = &cobra.Command{
		Use:   "unit",
		Short: "Apex plant unit service",
		Run:   unit,
	}
)

func init() {
	rootCmd.AddCommand(unitCmd)

	// TODO: make mandatory
	unitCmd.PersistentFlags().StringVarP(&name, "name", "n", "", "the unit service name to launch")
	viper.BindPFlag("unit", unitCmd.PersistentFlags().Lookup("unit"))
}

func unit(cmd *cobra.Command, args []string) {
	unit, err := service.NewUnit(name)
	if err != nil {
		panic(err)
	}
	unit.Start()
}
