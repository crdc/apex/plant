package service

import (
	"context"

	gcontext "gitlab.com/crdc/apex/plant/pkg/context"

	"gitlab.com/crdc/apex/go-apex/log"

	kitlog "github.com/go-kit/kit/log"
)

type keyUnitContext int

const (
	keyConfig keyUnitContext = iota + 1
	keyLog
	keyBroker
	keyRelay
)

type Unit struct {
	name   string
	config *gcontext.Config
	logger *kitlog.Logger
	broker *Broker
	relay  *Relay
}

func NewUnit(name string) (*Unit, error) {
	config, err := gcontext.LoadConfig("configs", name)
	if err != nil {
		return nil, err
	}
	//logger := NewLogger(config)
	logger := GetLogger()

	ctx := context.Background()

	broker, _ := NewBroker(logger)
	broker.Bind(config.Unit.Endpoint)

	relay := NewRelay(config, logger)

	// Add services to context
	ctx = context.WithValue(ctx, keyConfig, config)
	ctx = context.WithValue(ctx, keyLog, logger)
	ctx = context.WithValue(ctx, keyBroker, broker)
	ctx = context.WithValue(ctx, keyRelay, relay)

	return &Unit{
		name:   name,
		config: config,
		logger: logger,
		broker: broker,
		relay:  relay,
	}, nil
}

func (u *Unit) Start() {
	doneBroker := make(chan bool, 1)
	doneRelay := make(chan bool, 1)

	go u.broker.Run(doneBroker)
	go u.relay.Run(doneRelay)

	// TODO: send exit to client if service quit received

	<-doneBroker
	<-doneRelay

	log.Info(u.logger, "msg", "unit shutting down")
}
