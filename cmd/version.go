package cmd

import (
	"fmt"

	"gitlab.com/crdc/apex/plant/pkg"

	"github.com/spf13/cobra"
)

func init() {
	rootCmd.AddCommand(versionCmd)
}

var versionCmd = &cobra.Command{
	Use:   "version",
	Short: "Print the version number of plantd-unit",
	Long:  `Plantd unit service version information.`,
	Run: func(cmd *cobra.Command, args []string) {
		fmt.Println(version.VERSION)
	},
}
