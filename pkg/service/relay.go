package service

import (
	"fmt"

	"gitlab.com/crdc/apex/plant/pkg/context"
	"gitlab.com/crdc/apex/plant/pkg/worker"

	"gitlab.com/crdc/apex/go-apex/api"
	"gitlab.com/crdc/apex/go-apex/log"

	kitlog "github.com/go-kit/kit/log"
)

// XXX: should this be named "Service" and handle all requests between this
// and an additional channel shared with the handler?

/*
 *type Service struct {
 *    // ???
 *}
 */

type Relay struct {
	broker   string
	endpoint string
	service  string
	logger   *kitlog.Logger
	buses    map[string]*Bus
	modules  map[string]*api.Client
}

func NewRelay(config *context.Config, logger *kitlog.Logger) *Relay {
	var buses map[string]*Bus = make(map[string]*Bus)
	var modules map[string]*api.Client = make(map[string]*api.Client)

	for name, bus := range config.Buses {
		log.Info(logger, "msg", "adding message bus",
			"bus", name,
			"frontend", bus.Frontend,
			"backend", bus.Backend,
			"capture", bus.Capture)
		buses[name] = NewBus(name, config.App, config, logger)
	}

	for name, module := range config.Modules {
		var err error
		log.Info(logger,
			"msg", "adding unit module",
			"module", name,
			"description", module.Description,
			"endpoint", module.Endpoint)
		modules[name], err = api.NewClient(module.Endpoint)
		if err != nil {
			log.Error(logger, "msg", "failed to create mdp client", "module", name, "err", err)
		}
	}

	return &Relay{
		broker:   config.Broker.Endpoint,
		endpoint: config.Unit.Endpoint,
		service:  config.App,
		logger:   logger,
		buses:    buses,
		modules:  modules,
	}
}

func (r *Relay) Run(done chan bool) {
	doneBuses := make(map[string](chan bool))
	doneModules := make(map[string](chan bool))
	moduleRequests := make(map[string](chan []byte))
	moduleResponses := make(map[string](chan []byte))

	for name, bus := range r.buses {
		doneBuses[name] = make(chan bool, 1)
		go bus.Run(doneBuses[name])
	}

	for name, module := range r.modules {
		doneModules[name] = make(chan bool, 1)
		moduleRequests[name] = make(chan []byte, 2)
		moduleResponses[name] = make(chan []byte, 2)
		go r.plantClient(module, name, doneModules[name], moduleRequests[name], moduleResponses[name])
	}

	handler := worker.NewHandler(moduleRequests, moduleResponses, r.logger)
	w, err := worker.NewWorker(r.broker, r.service, handler, r.logger)
	if err != nil {
		log.Error(r.logger, "msg", err)
		done <- true
	}

	go w.Run()
	if err != nil {
		log.Error(r.logger, "msg", err)
		done <- true
	}

	// FIXME: nothing waits for doneModules[] channels

	done <- true
}

// This client connects to the broker service
//
// TODO: pass in a service statistics channel to record eg. error count, MPS
// FIXME: the client is already available at r.modules[name]
func (r *Relay) plantClient(client *api.Client, name string, done chan bool, req <-chan []byte, resp chan<- []byte) {
	logger := *GetLogger()

	for {
		// Get message from the request channel
		msg := <-req
		body := <-req

		//log.Info(r.logger, "msg", "received message on channel", "module", name, "data", string(msg))

		request := make([]string, 2)
		request[0] = string(msg)
		request[1] = string(body)

		// Send it to the module using the plant client
		err := client.Send(name, request...)
		if err != nil {
			log.Error(r.logger, "msg", "error during send", "body", err)
			continue
		}

		// Wait for the module's response
		response, err := client.Recv()
		if err != nil {
			// XXX: consider filling actual response type here
			logger.Log("msg", "error during response", "request", request, "response", response, "error", err)
			log.Error(r.logger, "msg", "error during receive", "body", err)
			resp <- []byte("error")
			re := fmt.Sprintf("%s", err)
			resp <- []byte(re)
			continue
		}

		log.Debug(r.logger, "msg", "!!!!!", "response size", len(response))
		log.Debug(r.logger, "msg", "!!!!!", "response", response[0])
		log.Debug(r.logger, "msg", "!!!!!", "response", response[1])
		//log.Info(r.logger, "msg", "received message response", "call", response[1], "data", response[2])

		// Send it back to the message handler
		// XXX: this should handle all responses, not just the first
		resp <- []byte(response[0])
		resp <- []byte(response[1])
		//resp <- []byte(response[2])
	}

	done <- true
}
