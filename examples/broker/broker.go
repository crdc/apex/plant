package main

import (
	"gitlab.com/crdc/apex/plant/pkg/service"

	"gitlab.com/crdc/apex/go-apex/log"

	"github.com/go-kit/kit/log/level"
)

func main() {
	logger := *log.GetLogger()
	logger = level.NewFilter(logger, level.AllowAll())

	broker, _ := service.NewBroker(logger)
	broker.Bind("tcp://*:5555")

	done := make(chan bool, 1)
	go broker.Run(done)

	<-done

	log.Info(&logger, "Interrupt received, shutting down...")
}
