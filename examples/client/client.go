package main

import (
	"encoding/json"

	"gitlab.com/crdc/apex/go-apex/api"
	"gitlab.com/crdc/apex/go-apex/log"

	"github.com/go-kit/kit/log/level"
)

type Message struct {
	Name string
}

func main() {
	logger := *log.GetLogger()
	logger = level.NewFilter(logger, level.AllowAll())

	session, _ := api.NewClient("tcp://localhost:5555")

	msg := &Message{Name: "example"}
	b, err := json.Marshal(msg)
	if err != nil {
		log.Error(&logger, "err", err)
		return
	}

	var count int
	for count = 0; count < 100000; count++ {
		err = session.Send("echo", string(b))
		if err != nil {
			log.Error(&logger, "msg", "send", "err", err)
			break
		}
	}
	for count = 0; count < 100000; count++ {
		_, err = session.Recv()
		if err != nil {
			log.Error(&logger, "msg", "recv", "err", err)
			break
		}
	}

	log.Info(&logger, "msg", "received replies", "count", count)
}
