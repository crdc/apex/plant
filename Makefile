PROJECT=plant
REGISTRY=registry.gitlab.com/crdc/apex/$(PROJECT)
DESTDIR = /usr
CONFDIR = /etc
SYSTEMDDIR = /lib/systemd

M := $(shell printf "\033[34;1m▶\033[0m")
TAG := $(shell git describe --all | sed -e's/.*\///g')

all: build

lint: ; $(info $M Linting the files)
	@./tools/checks lint

test: ; $(info $M Running unittests)
	@./tools/checks test

race: ; $(info $M Running data race detector)
	@./tools/checks race

msan: ; $(info $M Running memory sanitizer)
	@./tools/checks msan

coverage: ; $(info $M Generating global code coverage report)
	@./tools/coverage

coverhtml: ; $(info $M Generating global code coverage report in HTML)
	@./tools/coverage html

build: ; $(info $(M) Building plantd project...)
	@./tools/build -s

examples: ; $(info $(M) Building examples...)
	@./tools/build -e broker
	@./tools/build -e client
	@./tools/build -e worker

image: ; $(info $(M) Building application image...)
	@docker build -t $(REGISTRY) -f ./build/Dockerfile .

container: image ; $(info $(M) Running application container...)
	@docker run -p 7201-7205:7201-7205 $(REGISTRY):latest

# FIXME: this assumes master branch and os/arch, consider using build output
install: ; $(info $(M) Installing plantd unit services...)
	@install -Dm 755 bin/plant-$(TAG)-linux-amd64 "$(DESTDIR)/bin/plantd-unit"
	@install -Dm 644 README.md "$(DESTDIR)/share/doc/plantd/unit/README"
	@install -Dm 644 LICENSE "$(DESTDIR)/share/licenses/plantd/unit/COPYING"
	@mkdir -p "$(CONFDIR)/plantd/unit"
	@mkdir -p /run/plantd
	@install -Dm 644 configs/* "$(CONFDIR)/plantd/unit/"
	@install -Dm 644 init/plantd-unit@.service "$(SYSTEMDDIR)/system/plantd-unit@.service"
	@systemctl daemon-reload

uninstall: ; $(info $(M) Uninstalling plantd unit services...)
	@rm $(DESTDIR)/bin/plantd-unit

clean: ; $(info $(M) Removing generated files... )
	@rm -rf bin/

.PHONY: all lint test race msan coverage coverhtml build examples install uninstall clean
