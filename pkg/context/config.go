package context

import (
	"fmt"
	"strings"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/viper"
)

type broker struct {
	Endpoint string `mapstructure:"endpoint"`
}

type unit struct {
	Endpoint          string `mapstructure:"endpoint"`
	HeartbeatLiveness int    `mapstructure:"heartbeat-liveness"`
	HeartbeatInterval int    `mapstructure:"heartbeat-interval"`
}

type log struct {
	Debug bool   `mapstructure:"debug"`
	Level string `mapstructure:"level"`
}

type bus struct {
	Frontend string `mapstructure:"frontend"`
	Backend  string `mapstructure:"backend"`
	Capture  string `mapstructure:"capture"`
}

type module struct {
	Endpoint    string `mapstructure:"endpoint"`
	Description string `mapstructure:"description"`
}

type Config struct {
	App     string            `mapstructure:"app"`
	Broker  broker            `mapstructure:"broker"`
	Unit    unit              `mapstructure:"unit"`
	Log     log               `mapstructure:"log"`
	Buses   map[string]bus    `mapstructure:"buses"`
	Modules map[string]module `mapstructure:"modules"`
}

func LoadConfig(path, name string) (*Config, error) {
	home, err := homedir.Dir()
	if err != nil {
		return nil, err
	}

	config := viper.New()
	config.SetConfigName(name)
	config.AddConfigPath("/etc/plantd/unit")
	config.AddConfigPath(".")
	config.AddConfigPath(path)
	config.AddConfigPath(fmt.Sprintf("%s/.config/plantd/unit", home))

	err = config.ReadInConfig()
	if err != nil {
		return nil, err
	}

	config.SetEnvPrefix("PLANTD_UNIT")
	config.SetEnvKeyReplacer(strings.NewReplacer(".", "_"))
	config.AutomaticEnv()

	var c Config

	err = config.Unmarshal(&c)
	if err != nil {
		return nil, err
	}

	return &c, nil
}
