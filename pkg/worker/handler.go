package worker

import (
	"bytes"
	"fmt"

	/*
	 *"gitlab.com/crdc/apex/plant/pkg/service"
	 */

	"gitlab.com/crdc/apex/go-apex/log"
	pb "gitlab.com/crdc/apex/go-apex/proto/v1"

	kitlog "github.com/go-kit/kit/log"
	"github.com/golang/protobuf/jsonpb"
)

type Handler struct {
	/*
	 *service   *service.Service
	 */
	requests  map[string](chan []byte)
	responses map[string](chan []byte)
	logger    *kitlog.Logger
}

func NewHandler(requests map[string](chan []byte), responses map[string](chan []byte), logger *kitlog.Logger) *Handler {
	return &Handler{
		requests,
		responses,
		logger,
	}
}

// TODO: not implemented, just stubbed
func (h *Handler) GetConfiguration(body string) ([]byte, error) {
	// XXX: currently using request used with configure service, this could be empty
	request := &pb.ConfigurationRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request with real data

	// TODO: use service config
	configuration := &pb.Configuration{}
	response := &pb.ConfigurationResponse{
		Configuration: configuration,
	}

	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetStatus(StatusRequest) returns (StatusResponse);
func (h *Handler) GetStatus(body string) ([]byte, error) {
	request := &pb.StatusRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request with real data

	// XXX: don't actually care about the body in this one if it's a pb.Empty
	details := make(map[string]string)
	details["test-key"] = "test-value"

	// TODO: get actual status response
	status := &pb.Status{
		Enabled: true,
		Loaded:  true,
		Active:  true,
		Details: details,
	}
	response := &pb.StatusResponse{
		Status: status,
	}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetSettings(SettingsRequest) returns (SettingsResponse);
func (h *Handler) GetSettings(body string) ([]byte, error) {
	// SettingsRequest is empty
	request := &pb.SettingsRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	settings := make(map[string]string)
	settings["test-key"] = "test-value"

	response := &pb.SettingsResponse{
		Id:       "1234",
		Settings: settings,
	}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetChannel(ChannelRequest) returns (ChannelResponse);
func (h *Handler) GetChannel(body string) ([]byte, error) {
	request := &pb.ChannelRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.ChannelResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetChannels(Empty) returns (ChannelsResponse);
func (h *Handler) GetChannels(body string) ([]byte, error) {
	request := &pb.Empty{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.ChannelsResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetJob(JobRequest) returns (JobResponse);
func (h *Handler) GetJob(body string) ([]byte, error) {
	request := &pb.JobRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetJobs(Empty) returns (JobsResponse);
func (h *Handler) GetJobs(body string) ([]byte, error) {
	request := &pb.Empty{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobsResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetJobStatus(JobRequest) returns (JobStatusResponse);
func (h *Handler) GetJobStatus(body string) ([]byte, error) {
	request := &pb.JobRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobStatusResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetModule(ModuleRequest) returns (ModuleResponse);
func (h *Handler) GetModule(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.ModuleResponse{
		Module: &pb.Module{
			Id:          "acquire-genicam",
			Name:        "genicam",
			ServiceName: "acquire",
		},
	}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc GetModules(Empty) returns (ModulesResponse);
func (h *Handler) GetModules(body string) ([]byte, error) {
	request := &pb.Empty{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.ModulesResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc TryModuleConnect(ModuleRequest) returns (JobResponse);
func (h *Handler) TryModuleConnect(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc TryModuleDisconnect(ModuleRequest) returns (JobResponse);
func (h *Handler) TryModuleDisconnect(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc TryModuleLoad(ModuleRequest) returns (JobResponse);
func (h *Handler) TryModuleLoad(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc TryModuleUnload(ModuleRequest) returns (JobResponse);
func (h *Handler) TryModuleUnload(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc TryModuleReload(ModuleRequest) returns (JobResponse);
func (h *Handler) TryModuleReload(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc TryModuleEnable(ModuleRequest) returns (JobResponse);
func (h *Handler) TryModuleEnable(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

// TODO: not implemented, just stubbed
// rpc TryModuleDisable(ModuleRequest) returns (JobResponse);
func (h *Handler) TryModuleDisable(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	// TODO: fulfil the request

	response := &pb.JobResponse{}
	b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
	if err := marshaler.Marshal(&b, response); err != nil {
		return nil, err
	}
	return b.Bytes(), nil
}

func (h *Handler) GetModuleConfiguration(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle get-module-configuration", "module", module, "request", body)
	h.requests[module] <- []byte("get-configuration")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-configuration", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received configuration response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) GetModuleStatus(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle get-module-status", "module", module, "request", body)
	h.requests[module] <- []byte("get-status")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-status", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received status response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) GetModuleSettings(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle get-module-settings", "module", module, "request", body)
	h.requests[module] <- []byte("get-settings")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-settings", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received settings response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) GetModuleJob(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle get-module-job", "module", module, "request", body)
	h.requests[module] <- []byte("get-job")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-job", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received job response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) GetModuleJobs(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle get-module-jobs", "module", module, "request", body)
	h.requests[module] <- []byte("get-jobs")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-jobs", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received jobs response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) GetModuleActiveJob(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}

	module := request.Id
	log.Debug(h.logger, "msg", "handle get-module-active-job", "module", module, "request", body)
	h.requests[module] <- []byte("get-active-job")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-active-job", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received job response response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) ModuleCancelJob(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle module-cancel-job", "module", module, "request", body)
	h.requests[module] <- []byte("cancel-job")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-cancel-job", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received job response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) ModuleSubmitJob(body string) ([]byte, error) {
	request := &pb.ModuleJobRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle module-submit-job", "module", module, "request", body)
	h.requests[module] <- []byte("submit-job")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-submit-job", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received job response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) ModuleSubmitEvent(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle module-submit-event", "module", module, "request", body)
	h.requests[module] <- []byte("submit-event")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-submit-event", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received job response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) ModuleAvailableEvents(body string) ([]byte, error) {
	request := &pb.ModuleRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle module-available-events", "module", module, "request", body)
	h.requests[module] <- []byte("available-events")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-available-events", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received events response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) GetModuleProperty(body string) ([]byte, error) {
	request := &pb.PropertyRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle get-module-property", "module", module, "request", body)
	h.requests[module] <- []byte("get-property")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-property", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received property response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) SetModuleProperty(body string) ([]byte, error) {
	request := &pb.PropertyRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle set-module-property", "module", module, "request", body)
	h.requests[module] <- []byte("set-property")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "set-module-property", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received property response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) GetModuleProperties(body string) ([]byte, error) {
	request := &pb.PropertiesRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle get-module-properties", "module", module, "request", body)
	h.requests[module] <- []byte("get-properties")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "get-module-properties", string(eh))
		resp := &pb.PropertiesResponse{
			Error: &pb.Error{Code: 501, Message: string(eh)},
		}
		b, marshaler := bytes.Buffer{}, jsonpb.Marshaler{}
		if err := marshaler.Marshal(&b, resp); err != nil {
			log.Error(h.logger, "msg", "jsonpb marshal", "data", err)
			// XXX: same issue not returning something here, but this really shouldn't happen
			return nil, err
		}
		return b.Bytes(), re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received properties response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) SetModuleProperties(body string) ([]byte, error) {
	request := &pb.PropertiesRequest{}
	if err := jsonpb.Unmarshal(bytes.NewReader([]byte(body)), request); err != nil {
		return nil, err
	}
	module := request.Id
	log.Debug(h.logger, "msg", "handle set-module-properties", "module", module, "request", body)
	h.requests[module] <- []byte("set-properties")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "set-module-properties", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received property response", "module", module, "response", string(response))

	return response, nil
}

func (h *Handler) ModuleShutdown(module, body string) ([]byte, error) {
	log.Debug(h.logger, "msg", "handle module-shutdown", "module", module, "request", body)
	h.requests[module] <- []byte("shutdown")
	h.requests[module] <- []byte(body)
	head := <-h.responses[module]
	if string(head) == "error" {
		eh := <-h.responses[module]
		re := fmt.Errorf("%s (%s): %s", module, "module-shutdown", string(eh))
		return nil, re
	}
	response := <-h.responses[module]
	log.Debug(h.logger, "msg", "received events response", "module", module, "response", string(response))

	return response, nil
}
