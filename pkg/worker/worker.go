package worker

import (
	"errors"

	"gitlab.com/crdc/apex/go-apex/api"
	"gitlab.com/crdc/apex/go-apex/log"
	//"gitlab.com/crdc/apex/go-apex/model"

	kitlog "github.com/go-kit/kit/log"
)

//  The worker class defines a single worker instance:
type Worker struct {
	session  *api.Worker //  MDP worker session
	endpoint string      //  Broker binds to this endpoint
	handler  *Handler
	logger   *kitlog.Logger
}

func NewWorker(endpoint string, name string, handler *Handler, logger *kitlog.Logger) (worker *Worker, err error) {
	session, err := api.NewWorker(endpoint, name)
	if err != nil {
		return nil, err
	}

	//  Initialize broker state
	worker = &Worker{
		session:  session,
		endpoint: endpoint,
		handler:  handler,
		logger:   logger,
	}

	return worker, nil
}

func (w *Worker) Run() error {
	var err error
	var request, reply []string
	for {
		request, err = w.session.Recv(reply)
		if err != nil {
			return err
		}

		if len(request) == 0 {
			return errors.New("request has zero parts")
		}

		msgType := request[0]
		log.Info(w.logger, "msg", msgType)

		// Reset reply
		reply = []string{}
		multiPart := false

		for _, part := range request[1:] {
			log.Info(w.logger, "msg", "received message", "procedure", msgType, "request", part)
			var data []byte
			switch msgType {
			case "get-configuration":
				if data, err = w.handler.GetConfiguration(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-status":
				if data, err = w.handler.GetStatus(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-settings":
				if data, err = w.handler.GetSettings(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-channel":
				if data, err = w.handler.GetChannel(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-channels":
				if data, err = w.handler.GetChannels(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-job":
				if data, err = w.handler.GetJob(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-jobs":
				if data, err = w.handler.GetJobs(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-job-status":
				if data, err = w.handler.GetJobStatus(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-module":
				if data, err = w.handler.GetModule(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-modules":
				if data, err = w.handler.GetModules(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "try-module-connect":
				if data, err = w.handler.TryModuleConnect(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "try-module-disconnect":
				if data, err = w.handler.TryModuleDisconnect(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "try-module-load":
				if data, err = w.handler.TryModuleLoad(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "try-module-unload":
				if data, err = w.handler.TryModuleUnload(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "try-module-reload":
				if data, err = w.handler.TryModuleReload(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "try-module-enable":
				if data, err = w.handler.TryModuleEnable(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "try-module-disable":
				if data, err = w.handler.TryModuleDisable(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-module-configuration":
				if data, err = w.handler.GetModuleConfiguration(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-module-status":
				if data, err = w.handler.GetModuleStatus(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-module-settings":
				if data, err = w.handler.GetModuleSettings(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-module-job":
				if data, err = w.handler.GetModuleJob(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-module-jobs":
				if data, err = w.handler.GetModuleJobs(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-module-active-job":
				if data, err = w.handler.GetModuleActiveJob(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "module-cancel-job":
				if data, err = w.handler.ModuleCancelJob(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "module-submit-job":
				if data, err = w.handler.ModuleSubmitJob(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "module-submit-event":
				if data, err = w.handler.ModuleSubmitEvent(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "module-available-events":
				if data, err = w.handler.ModuleAvailableEvents(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-module-property":
				if data, err = w.handler.GetModuleProperty(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "set-module-property":
				if data, err = w.handler.SetModuleProperty(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "get-module-properties":
				if data, err = w.handler.GetModuleProperties(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				//break
			case "set-module-properties":
				if data, err = w.handler.SetModuleProperties(part); err != nil {
					log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
					break
				}
				break
			case "module-shutdown":
				if !multiPart {
					if data, err = w.handler.ModuleShutdown(part, "shutdown"); err != nil {
						log.Warn(w.logger, "msg", "message failed", "type", msgType, "body", err)
						break
					}
				}
				multiPart = true
				break
			default:
				// TODO:
				log.Error(w.logger, "msg", "invalid message type provided")
				break
			}

			// Append
			reply = append(reply, string(data))
			log.Debug(w.logger, "msg", "?????", "data", reply)
		}
	}

	return nil
}
