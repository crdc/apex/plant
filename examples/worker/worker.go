package main

import (
	"os"

	"gitlab.com/crdc/apex/go-apex/api"
	"gitlab.com/crdc/apex/go-apex/log"

	"github.com/go-kit/kit/log/level"
)

func main() {
	logger := *log.GetLogger()
	logger = level.NewFilter(logger, level.AllowAll())

	session, err := api.NewWorker("tcp://localhost:5555", "echo")
	if err != nil {
		os.Exit(1)
	}

	var request, reply []string
	for {
		request, err = session.Recv(reply)
		if err != nil {
			os.Exit(1)
		}

		// don't echo for test, just print
		reply = request
		log.Info(&logger, "Received:", request)
	}
}
