# Apex Plant Services

[![Build Status](https://gitlab.com/crdc/apex/plant/badges/master/build.svg)](https://gitlab.com/crdc/apex/plant/commits/master)
[![Coverage Report](https://gitlab.com/crdc/apex/plant/badges/master/coverage.svg)](https://gitlab.com/crdc/apex/plant/commits/master)
[![Go Report Card](https://goreportcard.com/badge/gitlab.com/crdc/apex/plant)](https://goreportcard.com/report/gitlab.com/crdc/apex/plant)
[![License MIT](https://img.shields.io/badge/License-MIT-brightgreen.svg)](https://img.shields.io/badge/License-MIT-brightgreen.svg)

[WIP] Beta state, does some stuff but needs work and will change.

## Setup

### Dependencies

Installation of these is independent of the distribution used.

* go
* ZeroMQ

### Apex/Plantd Dependencies

* plantd-master
* plantd-configure
* plantd-broker

### Build

```sh
make && sudo make install
```

or using Docker

```sh
make image
```

Fedora 29

```sh
sudo dnf install libsodium
git clone https://github.com/zeromq/libzmq.git
cd libzmq
./autogen.sh
./configure --prefix=/usr --with-libsodium
make
sudo make install
sudo ldconfig
cd ..
git clone https://github.com/zeromq/czmq.git
cd czmq
./autogen.sh
./configure --prefix=/usr
make
sudo make install
```

## Running

The application can either be run from the resulting `bin/` directory, or the
`systemd` service file can be used.

For example, to use the `acquire` unit:

```sh
sudo systemctl enable plantd-unit@acquire
sudo systemctl start plantd-unit@acquire
```

## Publishing

```sh
docker build -t registry.gitlab.com/plantd/unit:v1 -f ./build/Dockerfile .
docker push registry.gitlab.com/plantd/unit:v1
```
