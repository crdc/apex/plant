module gitlab.com/crdc/apex/plant

require (
	github.com/go-kit/kit v0.8.0
	github.com/golang/protobuf v1.3.1
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mitchellh/go-homedir v1.0.0
	github.com/op/go-logging v0.0.0-20160315200505-970db520ece7
	github.com/pebbe/zmq4 v1.0.0
	github.com/spf13/cobra v0.0.3
	github.com/spf13/viper v1.3.1
	github.com/zeromq/goczmq v4.1.0+incompatible
	gitlab.com/crdc/apex/go-apex v0.1.20
	golang.org/x/net v0.0.0-20190213061140-3a22650c66bd
	google.golang.org/grpc v1.19.0
)
