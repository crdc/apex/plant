package main

import (
	"gitlab.com/crdc/apex/plant/cmd"
)

func main() {
	cmd.Execute()
}
