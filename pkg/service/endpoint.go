package service

import (
	"gitlab.com/crdc/apex/plant/pkg/context"

	"gitlab.com/crdc/apex/go-apex/api"

	"github.com/op/go-logging"
)

// TODO: use this or get rid of it

type Endpoint struct {
	name   string
	client *api.Client
	config *context.Config
	log    *logging.Logger
}

func NewEndpoint(name string, client *api.Client, config *context.Config, log *logging.Logger) *Endpoint {
	return &Endpoint{
		name,
		client,
		config,
		log,
	}
}
