package service

import (
	//"fmt"
	//"strings"
	"time"

	"gitlab.com/crdc/apex/plant/pkg/context"

	"gitlab.com/crdc/apex/go-apex/log"

	kitlog "github.com/go-kit/kit/log"
	//zmq "github.com/pebbe/zmq4"
	czmq "github.com/zeromq/goczmq"
)

type Bus struct {
	name     string
	unit     string
	backend  string
	frontend string
	capture  string
	logger   *kitlog.Logger
}

func NewBus(name string, unit string, config *context.Config, logger *kitlog.Logger) *Bus {
	return &Bus{
		name:     name,
		unit:     unit,
		backend:  config.Buses[name].Backend,
		frontend: config.Buses[name].Frontend,
		capture:  config.Buses[name].Capture,
		logger:   logger,
	}
}

/*
 *func (b *Bus) frontend_thread() {
 *    frontend, _ := zmq.NewSocket(zmq.SUB)
 *    defer frontend.Close()
 *    if b.pubMode == "bind" {
 *        // FIXME: This doesn't cover all possible useful cases
 *        e := strings.Replace(b.backend, "*", "localhost", 1)
 *        log.Info(b.logger, "msg", "frontend thread", "connect", e)
 *        frontend.Connect(e)
 *    } else {
 *        e := strings.Replace(b.backend, "localhost", "*", 1)
 *        log.Info(b.logger, "msg", "frontend thread", "bind", e)
 *        frontend.Bind(e)
 *    }
 *
 *    filter := fmt.Sprintf("KA:%s:%s", b.name, b.unit)
 *    frontend.SetSubscribe(filter)
 *
 *    for {
 *        _, err := frontend.RecvMessage(0)
 *        if err != nil {
 *            break
 *        }
 *    }
 *}
 */

/*
 *func (b *Bus) backend_thread() {
 *    backend, _ := zmq.NewSocket(zmq.PUB)
 *    defer backend.Close()
 *    if b.subMode == "connect" {
 *        e := strings.Replace(b.frontend, "localhost", "*", 1)
 *        log.Info(b.logger, "msg", "backend thread", "bind", e)
 *        backend.Bind(e)
 *    } else {
 *        e := strings.Replace(b.frontend, "*", "localhost", 1)
 *        log.Info(b.logger, "msg", "backend thread", "connect", e)
 *        backend.Connect(e)
 *    }
 *
 *    for {
 *        s := fmt.Sprintf("KA:%s:%s", b.name, b.unit)
 *        _, err := backend.SendMessage(s)
 *        if err != nil {
 *            break
 *        }
 *        time.Sleep(1000 * time.Millisecond)
 *    }
 *}
 */

// FIXME: This is currently here to test keeping the socket alive
func (b *Bus) captureThread(done chan bool) {
	pipe, _ := czmq.NewPull(b.capture)

	log.Info(b.logger, "msg", "capture proxy messages", "bus", b.name)
	for {
		msg, err := pipe.RecvMessage()
		if err != nil {
			break // Interrupted
		}
		log.Debug(b.logger, "msg", "capture", "received", msg, "bus", b.name)
	}

	log.Info(b.logger, "msg", "capture ended", "bus", b.name)
	done <- true
}

func (b *Bus) Run(done chan bool) {
	doneCapture := make(chan bool, 1)

	//go b.frontend_thread()
	//go b.backend_thread()
	go b.captureThread(doneCapture)

	time.Sleep(100 * time.Millisecond)

	proxy := czmq.NewProxy()
	defer proxy.Destroy()
	if err := proxy.SetFrontend(czmq.XSub, b.frontend); err != nil {
		log.Error(b.logger, "msg", "failed to connect frontend to proxy", "endpoint", b.frontend, "bus", b.name)
		done <- true
	}
	log.Info(b.logger, "msg", "frontend connected", "endpoint", b.frontend, "bus", b.name)
	if err := proxy.SetBackend(czmq.XPub, b.backend); err != nil {
		log.Error(b.logger, "msg", "failed to connect backend to proxy", "endpoint", b.backend, "bus", b.name)
		done <- true
	}
	log.Info(b.logger, "msg", "backend connected", "endpoint", b.backend, "bus", b.name)
	if err := proxy.SetCapture(b.capture); err != nil {
		log.Error(b.logger, "msg", "failed to connect capture to proxy", "endpoint", b.capture, "bus", b.name)
		done <- true
	}
	log.Info(b.logger, "msg", "capture connected", "endpoint", b.capture, "bus", b.name)

	<-doneCapture

	log.Info(b.logger, "msg", "proxy stopped", "bus", b.name)
	done <- true
}
