package service

import (
	"os"
	"sync"

	//"gitlab.com/crdc/apex/plant/pkg/context"

	//"gitlab.com/crdc/apex/go-apex/log"

	kitlog "github.com/go-kit/kit/log"
	"github.com/go-kit/kit/log/level"
)

/*
 *func NewLogger(config *context.Config) *kitlog.Logger {
 *    logger := log.GetLogger()
 *    l := kitlog.With(*logger, "service", config.App)
 *    logger = &l
 *    log.SetLevel(logger, config.Log.Level)
 *
 *    return logger
 *}
 */

var instance *kitlog.Logger
var once sync.Once

func GetLogger() *kitlog.Logger {
	once.Do(func() {
		logger := kitlog.NewJSONLogger(kitlog.NewSyncWriter(os.Stdout))
		logger = kitlog.With(logger, "ts", kitlog.DefaultTimestampUTC)
		logger = level.NewFilter(logger, level.AllowAll())
		instance = &logger
	})
	return instance
}
