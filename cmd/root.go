package cmd

import (
	"fmt"
	"log"
	"os"

	homedir "github.com/mitchellh/go-homedir"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	cfgFile string

	rootCmd = &cobra.Command{
		Use:   "plant",
		Short: "Apex plant services",
		Long:  "Message queue brokers to relay messages between services and connected modules.",
	}
)

// Execute is called by cobra
func Execute() {
	if err := rootCmd.Execute(); err != nil {
		log.Fatal(err)
	}
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringVar(
		&cfgFile,
		"config",
		"",
		"config file (default is $HOME/.config/plantd/<unit>.yaml)",
	)
}

// initConfig reads in config file and ENV variables if set.
func initConfig() {
	if cfgFile != "" {
		// Use config file from the flag.
		viper.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		if err != nil {
			log.Println(err)
			os.Exit(1)
		}

		// Search config in home directory with name "config.yaml".
		viper.SetConfigName("config")
		viper.AddConfigPath(".")
		viper.AddConfigPath(fmt.Sprintf("%s/.config/plantd/unit", home))
		viper.AddConfigPath("/etc/plantd/unit")
	}

	viper.AutomaticEnv()

	err := viper.ReadInConfig()
	if err != nil {
		fmt.Errorf("fatal error reading config file: %s", err)
	} else {
		fmt.Println("using config file:", viper.ConfigFileUsed())
	}
}
